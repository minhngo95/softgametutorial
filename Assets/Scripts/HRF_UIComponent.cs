using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HRF_UIComponent : MonoBehaviour
{
    public abstract void OnComponentAwake();

    public abstract void SocketLink();

    public abstract void OnComponentCreate();

    public abstract void OnComponentUpdate();

    public abstract void SocketUnlink();

    public abstract void OnComponentDestroy();

}
