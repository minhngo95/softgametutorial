using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeControll : MonoBehaviour
{
    public GameObject Tree = null;
    public Transform[] TreeSlot = null;
    public Button btnTree = null;
    private void OnEnable()
    {
        btnTree.onClick.AddListener(OnTreeClick);
    }
    private void OnDestroy()
    {
        btnTree.onClick.RemoveListener(OnTreeClick);
    }
    private void OnTreeClick()
    {
        for (int i = 0; i < TreeSlot.Length; i++)
        {
            if (TreeSlot[i].childCount == 0)
            {
                GameControll.BallIsChoose.SetParent(TreeSlot[i]);
                ((RectTransform)GameControll.BallIsChoose.transform).anchoredPosition = Vector2.zero;
            }
        }
    }    
}
