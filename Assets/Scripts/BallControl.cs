using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    [SerializeField] public
    Toggle BallBtn = null;
    [SerializeField] public
    Image Activeimg = null;
    public Image BallImage = null;
    public int BallIndex;
  

    private void OnEnable()
    {
        BallBtn.onValueChanged.AddListener(ActiveChoose);
    }
    private void OnDestroy()
    {
        BallBtn.onValueChanged.RemoveListener(ActiveChoose);
    }

    public void SettingBall(int BallIndex,Color BallColor)
    {
        this.BallIndex = BallIndex;
        BallImage.color = BallColor;
    }
    private void ActiveChoose(bool IsActive)
    {
        Activeimg.gameObject.SetActive(IsActive);
        if (IsActive)
        {
            GameControll.BallIsChoose = transform;
        }
    }   
   
}    

