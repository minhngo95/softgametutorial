﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

 public class GameControll : MonoBehaviour
{
    [SerializeField]
    TreeControll TreeSource = null;
    [SerializeField]
    BallControl BallSource = null;
    [SerializeField]
    Color[] BallColor = null;
    [SerializeField]
    public ToggleGroup ToggleGroup = null;
    private Toggle newToggle = null;

    [SerializeField]
    Button moreTree = null;
    [SerializeField]
    Transform[] TreeParent = null;

    List<TreeControll> ListTree = new List<TreeControll>();
    List<BallControl> ListBall = new List<BallControl>();

    [SerializeField] int StartTree; // Số lượng Tree Ban đầu
    [SerializeField] int TreeEmpty; // Số lượng Tree rỗng Ban đầu

    public static Transform BallIsChoose;
    private void OnEnable()
    {
        for (int i = 0; i < StartTree; i++)
        {
            CreateTree();
        }

        for (int i = 0; i < TreeEmpty; i++)
        {
            CreateTree();
        }

        InitBall();
        moreTree.onClick.AddListener(CreateTree);
       
       
    }
    private void OnDestroy()
    {
        moreTree.onClick.RemoveListener(CreateTree);
        
    }
    private void Update()
    {
        DetroyObjSame();
    }
    int CountTree = 0;
    private void CreateTree()
    {
       TreeControll treeControll = Instantiate(TreeSource, TreeParent[CountTree % 2]);
        treeControll.btnTree.onClick.AddListener(DetroyObjSame);
       ListTree.Add(treeControll);
       CountTree++;
    }
    private void InitBall()
    {

        for (int i = 0; i < StartTree; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                BallControl ballControl = Instantiate(BallSource);
                ballControl.SettingBall(i, BallColor[i]);
                ListBall.Add(ballControl);
                //get toggle group
                newToggle = ballControl.GetComponentInChildren<Toggle>();
                newToggle.group = ToggleGroup;
             
            }
        }

        Shuffle(ListBall);

        int count = 0;
        for (int i = 0; i < StartTree; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                ListBall[count].transform.SetParent(ListTree[i].TreeSlot[j]);
                ((RectTransform)ListBall[count].transform).anchoredPosition = Vector2.zero;
                count++;
            }
        }
    } 
    private  System.Random rng = new System.Random();

    public void Shuffle(List<BallControl> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            BallControl value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    private void DetroyObjSame()
    {
        Dictionary<Transform, List<BallControl>> aa = new Dictionary<Transform, List<BallControl>>();
        for (int i = 0; i < ListBall.Count; i++)
        {
            if (aa.ContainsKey(ListBall[i].transform.parent.parent))
            {
                aa[ListBall[i].transform.parent.parent].Add(ListBall[i]);
            }
            else
            {
                aa.Add(ListBall[i].transform.parent.parent, new List<BallControl> { ListBall[i] });
            }
        }

        foreach (var item in aa.Values)
        {
            bool hidenAllBall = true;
            if (item.Count > 3)
            {
                for (int j = 0; j < item.Count - 1; j++)
                {
                    if (item[j].BallIndex != item[j + 1].BallIndex)
                    {
                        hidenAllBall = false;
                    }
                }
            }
            else
            {
                hidenAllBall = false;
            }

            if (hidenAllBall)
            {
                for (int j = 0; j < item.Count; j++)
                {
                    item[j].gameObject.SetActive(false);
                }
            }

        }
    }

}
