using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum2 : MonoBehaviour
{
    [SerializeField] AnimationCurve RotaPen;
    private float CountTime;
 
    void Update()
    {
        CountTime += Time.deltaTime;
        transform.localEulerAngles = new Vector3(RotaPen.Evaluate(CountTime), 0, 0);
        
    }
}
